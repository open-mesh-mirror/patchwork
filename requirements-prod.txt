Django~=3.2.0
djangorestframework~=3.12.0
django-filter~=21.1.0
psycopg2-binary~=2.8.0
sqlparse~=0.4.0
